# DSX dependent DEG Nasonia Table

Supplementary table 1: Excel file containing the main results of our study regarding DSX role in sex-biased gene regulation. 
For each gene is reported the sex bias and the effects that dsx knockdown has on its expression in males and females, at each developmental stage and tissue we analysed. Moreover, we report for each gene the presence of a consensus DSX binding sequence of either 9 or 11 nucleotides. The information from these analyses was integrated to define a regulatory mode for DSX, as explained in Supplementary Figure 3; the regulatory mode is also reported in this table. Lastly, we include information regarding DSX regulation of the Drosophila melanogaster homolog according to Clough et al., (2014).
